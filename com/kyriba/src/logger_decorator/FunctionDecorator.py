from functools import wraps


def decorate_functions(function_decorator):
    def decorator(cls):
        for name, obj in vars(cls).items():
            if callable(obj):
                try:
                    obj = obj.__func__
                except AttributeError:
                    pass
                setattr(cls, name, function_decorator(obj))
        return cls

    return decorator


def print_calling(func):
    @wraps(func)
    def wrapper(*args, **kw):
        try:
            res = func(*args, **kw)
        finally:
            print('{} has called'.format(func.__name__))
        return res

    return wrapper
