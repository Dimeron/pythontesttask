import argparse
import os
from parsers import class_list

extentions = {"txt": "BaseParser", "rem": "BaseParser", "xml": "BaseParser"}

parser = argparse.ArgumentParser(prog='FileParser', description='Process arguments.', usage='%(prog)s [options]')

parser.add_argument('files', metavar='S', type=str, nargs=1, help='an files to parse')

args = parser.parse_args()
_, extention = os.path.splitext(args.files[0])
extention = extention[1:].capitalize()
# extention = args.files[0].splitext.]capitalize() + "Parser"
prs_class = class_list.get(extention + "Parser")
prs_type = prs_class()

print("Output for {} parser is: {}".format(extention, prs_type.count_rows("storage/" + args.files[0])))
