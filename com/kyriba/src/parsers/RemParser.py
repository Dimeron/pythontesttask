from .BaseParser import BaseParser
from logger_decorator import FunctionDecorator


@FunctionDecorator.decorate_functions(FunctionDecorator.print_calling)
class RemParser(BaseParser):

    def __init__(self):
        pass

    def count_rows(self, filename):
        with open(filename) as f:
            for i, l in enumerate(f):
                pass
        return "Rem: {}".format(str(i))
