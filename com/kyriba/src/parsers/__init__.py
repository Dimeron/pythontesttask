import importlib
import os
import pkgutil

from parsers.BaseParser import BaseParser

import parsers

pkg_dir = os.path.dirname(__file__)
for (module_loader, name, ispkg) in pkgutil.iter_modules([pkg_dir]):
    importlib.import_module('.' + name, __package__)

class_list = {cls.__name__: cls for cls in [parsers.BaseParser] + parsers.BaseParser.__subclasses__()}

print(class_list)
